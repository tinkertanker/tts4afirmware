// NEW IN VERSION 1.5:
// Changed pin 8 from standard servo to normal digital output

// NEW IN VERSION 1.4:
// Changed Serial.print() for Serial.write() in ScratchBoardSensorReport function to make it compatible with latest Arduino IDE (1.0)

// NEW IN VERSION 1.3:
// Now it works on GNU/Linux. Also tested with MacOS and Windows 7.
// timer2 set to 20ms, fixing a glitch that made this period unstable in previous versions.
// readSerialport() function optimized.
// pulse() modified so that it receives pulse width as a parameter instead using a global variable.
// updateServoMotors changes its name as a global variable had the same name.
// Some minor fixes.

// Thanks to Jorge Gomez for all these new fixes!

#define TIMER2_PRELOAD 100

#define FEATURE_LCD          1
#define FEATURE_RANGEFINDER  2
#define FEATURE_TOUCH        4

#define RANGEFINDER_TRIGGER_PIN  10
#define RANGEFINDER_ECHO_PIN     11
#define RANGEFINDER_ECHO_TIMEOUT 3000

#include <CapacitiveSensor.h>
#include <Wire.h> 
#include <LiquidCrystal_I2C.h>

LiquidCrystal_I2C lcd(0x27, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address

CapacitiveSensor touchSensors[] = {
  CapacitiveSensor(7,2),
  CapacitiveSensor(7,3),
  CapacitiveSensor(7,4)
};
int touchSensorCount = 3;
int touchSensorStartSensorIndex = 6;
int touchSensorTimeout = 3;

char outputs[13];
int states[13];
int features;

unsigned long initialPulseTime;
unsigned long lastDataReceivedTime;

volatile boolean updateServoMotors;
volatile boolean newInterruption;

void setup()
{
 features = 0; // turn off all optional features by default
 Serial.begin(38400);
 Serial.flush();
 configurePins();
 configureServomotors();
 lastDataReceivedTime = millis();
 
 for (int i=0; i < touchSensorCount; i++) {
   touchSensors[i].set_CS_Timeout_Millis(touchSensorTimeout);
 }
}

void loop()
{
 if (updateServoMotors)
 {
   sendUpdateServomotors();
   sendSensorValues();
   updateServoMotors = false;
 }
 else
 {
   readSerialPort();
 }
}

void configurePins()
{
 for (int index = 0; index < 13; index++)
 {
    states[index] = 0; // reset pins, features and LCD
 }
 
 for (int index = 4; index < 14; index++)
 {
    pinMode(index, OUTPUT);
    digitalWrite(index, LOW); // reset pins
 }

 pinMode(2,INPUT);
 digitalWrite(2, HIGH);
 pinMode(3,INPUT);
 digitalWrite(3, HIGH);
 pinMode(4,INPUT);
 digitalWrite(4, HIGH);
 
 // Definition
 // c: continuous servo
 // a: analog/PWM
 // s: regular servo
 // d: digital output
 // l: LCD cursor
 // L: LCD print
 // f: configure featureset
 outputs[0] = 'x'; //pin 4 - unused
 outputs[1] = 'a'; //pin 5
 outputs[2] = 'a'; //pin 6
 outputs[3] = 's'; //pin 7
 outputs[4] = 's'; //pin 8
 outputs[5] = 'a'; //pin 9
 outputs[6] = 'd'; //pin 10
 outputs[7] = 'd'; //pin 11
 outputs[8] = 'd'; //pin 12
 outputs[9] = 'd'; //pin 13
 outputs[10] = 'l'; // LCD Cursor
 outputs[11] = 'L'; // LCD Character
 outputs[12] = 'f'; // define featureset
}

void configureServomotors() //servomotors interruption configuration (interruption each 10 ms on timer2)
{
 newInterruption = false;
 updateServoMotors = false;

 TCCR2A = 0;
 TCCR2B = 1<<CS22 | 1<<CS21 | 1<<CS20;
 TIMSK2 = 1<<TOIE2; //timer2 Overflow Interrupt
 TCNT2 = TIMER2_PRELOAD; //start timer
}

void sendSensorValues()
{
  int sensorValues[6], readings[5], sensorIndex;
    for (sensorIndex = 0; sensorIndex < 6; sensorIndex++) //for analog sensors, calculate the median of 5 sensor readings in order to avoid variability and power surges
    {
      for (int p = 0; p < 5; p++)
        readings[p] = analogRead(sensorIndex);
      InsertionSort(readings, 5); //sort readings
      sensorValues[sensorIndex] = readings[2]; //select median reading
    }

    //send analog sensor values
    for (sensorIndex = 0; sensorIndex < 6; sensorIndex++)
      ScratchBoardSensorReport(sensorIndex, sensorValues[sensorIndex]);

    //send digital sensor values
    if (features & FEATURE_TOUCH) {
      for (int i=0; i < touchSensorCount; i++) {
        long reading = touchSensors[i].capacitiveSensor(30);
        ScratchBoardSensorReport(i + touchSensorStartSensorIndex, (reading > 100)?1023:0);
      }
    } else {
      ScratchBoardSensorReport(6, digitalRead(2)?1023:0);
      ScratchBoardSensorReport(7, digitalRead(3)?1023:0);
      ScratchBoardSensorReport(8, digitalRead(4)?1023:0);
    }
    
    if (features & FEATURE_RANGEFINDER) {
      digitalWrite(RANGEFINDER_TRIGGER_PIN, LOW); 
      delayMicroseconds(2);     
      digitalWrite(RANGEFINDER_TRIGGER_PIN, HIGH); 
      delayMicroseconds(5);
      digitalWrite(RANGEFINDER_TRIGGER_PIN, LOW);
      long duration = pulseIn(RANGEFINDER_ECHO_PIN, HIGH, RANGEFINDER_ECHO_TIMEOUT);
      if (duration == 0) {
        duration = RANGEFINDER_ECHO_TIMEOUT;
      }
      long mm = duration * 10 / 29 / 2;
      ScratchBoardSensorReport(9, mm);
    }
}

void InsertionSort(int* array, int n)
{
  for (int i = 1; i < n; i++)
    for (int j = i; (j > 0) && ( array[j] < array[j-1] ); j--)
      swap( array, j, j-1 );
}

void swap (int* array, int a, int b)
{
  int temp = array[a];
  array[a] = array[b];
  array[b] = temp;
}

void ScratchBoardSensorReport(int sensor, int value) //PicoBoard protocol, 2 bytes per sensor
{
  Serial.write( B10000000
                | ((sensor & B1111)<<3)
                | ((value>>7) & B111));
  Serial.write( value & B1111111);
}

void readSerialPort()
{
  int pin, inByte, sensorHighByte;

  if (Serial.available() > 1)
  {
    lastDataReceivedTime = millis();
    inByte = Serial.read();

    if (inByte >= 128) // Are we receiving the word's header?
    {
      sensorHighByte = inByte;
      pin = ((inByte >> 3) & 0x0F) + 4;
      while (!Serial.available()); // Wait for the end of the word with data
      inByte = Serial.read();
      if (inByte <= 127) // This prevents Linux ttyACM driver to fail
      {
        states[pin - 4] = ((sensorHighByte & 0x07) << 7) | (inByte & 0x7F);
          updateActuator(pin - 4);
      }
    }
  }
  else checkScratchDisconnection();
}

void reset() //with xbee module, we need to simulate the setup execution that occurs when a usb connection is opened or closed without this module
{
  for (int pos = 0; pos < 10; pos++)  //stop all actuators
  {
    states[pos] = 0;
    digitalWrite(pos + 2, LOW);
  }

  //reset servomotors
  newInterruption = false;
  updateServoMotors = false;
  TCNT2 = TIMER2_PRELOAD;

  //protocol handshaking
  sendSensorValues();
  lastDataReceivedTime = millis();
}

void updateActuator(int pinNumber)
{
  if (outputs[pinNumber] == 'f') {
    // adjust featureset
    int newFeatures = states[pinNumber];
    int toggleFeatures = newFeatures ^ features;
    if (toggleFeatures & FEATURE_LCD) {
      if (newFeatures & FEATURE_LCD) {
        // turn on LCD
        lcd.begin(16,2);               // initialize the lcd 
        lcd.home();                   // go home
        lcd.print(" Get Hacking");  
      } else {
        // turn off LCD
        lcd.clear();
        lcd.print("LCD OFF");
      }
    }
    if (toggleFeatures & FEATURE_RANGEFINDER) {
      if (newFeatures & FEATURE_RANGEFINDER) {
        // turn on rangefinder
        pinMode(RANGEFINDER_ECHO_PIN, INPUT);
      } else {
        // turn off rangefinder
        pinMode(RANGEFINDER_ECHO_PIN, OUTPUT);
      }
    }
    if (toggleFeatures & FEATURE_TOUCH) {
      if (newFeatures & FEATURE_TOUCH) {
        // turn on touch
      } else {
        // turn off touch
      }
    }
    features = newFeatures;
  } else if (outputs[pinNumber] == 'l') {
    if (features & FEATURE_LCD) {
      lcd.setCursor((states[pinNumber]-1) % 16,(states[pinNumber]-1) / 16);
    }
  } else if (outputs[pinNumber] == 'L') {
    if (features & FEATURE_LCD) {
      char myChar = states[pinNumber];
      if (myChar == 27) {
        lcd.clear(); // used for clearLCD block
      } else {
        lcd.print(myChar);
      }
    }
  } else if (outputs[pinNumber] == 'd') {
    digitalWrite(pinNumber + 4, states[pinNumber]);
  } else if (outputs[pinNumber] == 'a') {
    analogWrite(pinNumber + 4, states[pinNumber]);
  }  
}

void sendUpdateServomotors()
{
  for (int p = 0; p < 10; p++)
  {
    if (outputs[p] == 'c') servomotorC(p + 4, states[p]);
    if (outputs[p] == 's') servomotorS(p + 4, states[p]);
  }
}

void servomotorC (int pinNumber, int dir)
{
  if (dir == 1) pulse(pinNumber, 1300); //clockwise rotation
  else if (dir == 2) pulse(pinNumber, 1700); //anticlockwise rotation
}

void servomotorS (int pinNumber, int angle)
{
  if (angle < 0) pulse(pinNumber, 600);
  else if (angle > 180) pulse(pinNumber, 2400);
  else pulse(pinNumber, (angle * 10) + 600);
}

void pulse (int pinNumber, int pulseWidth)
{
  initialPulseTime = micros();
  digitalWrite(pinNumber, HIGH);

  while (micros() < pulseWidth + initialPulseTime){}
  digitalWrite(pinNumber, LOW);
}

void checkScratchDisconnection() //the reset is necessary when using an wireless arduino board (because we need to ensure that arduino isn't waiting the actuators state from Scratch) or when scratch isn't sending information (because is how serial port close is detected)
{
  if (millis() - lastDataReceivedTime > 1000) reset(); //reset state if actuators reception timeout = one second
}

ISR(TIMER2_OVF_vect) //timer1 overflow interrupt vector handler
{ //timer2 => 8 bits counter => 256 clock ticks
  //preeescaler = 1024 => this routine is called 61 (16.000.000/256/1024) times per second approximately => interruption period =  1 / 16.000.000/256/1024 = 16,384 ms
  //as we need a 20 ms interruption period but timer2 doesn't have a suitable preescaler for this, we program the timer with a 10 ms interruption period and we consider an interruption every 2 times this routine is called.
  //to have a 10 ms interruption period, timer2 counter must overflow after 156 clock ticks => interruption period = 1 / 16.000.000/156/1024 = 9,984 ms => counter initial value (TCNT) = 100
  if (newInterruption)
  {
    updateServoMotors = true;
  }
  newInterruption = !newInterruption;
  TCNT2 = TIMER2_PRELOAD;  //reset timer
}
